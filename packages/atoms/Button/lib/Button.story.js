import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Button from './Button'

storiesOf('Button', module)
  .add('with text', () =>
    (
      <Button onClick={action('clicked')}>This is a button</Button>
    ),
  {
    notes: 'A very simple example of addon notes'
  }
  )
