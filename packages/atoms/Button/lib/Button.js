import React, { Component } from 'react'
import styles from './Button.scss'
import classnames from 'classnames'
import PropTypes from 'prop-types'

class Button extends Component {
  static propTypes = {
    appearance: PropTypes.oneOf([
      'primary',
      'secondary',
      'tertiary'
    ]),
    buttonStyle: PropTypes.oneOf([
      'solid',
      'outline'
    ]),
    disabled: PropTypes.bool
  }

  static defaultProps = {
    appearance: 'primary',
    buttonStyle: 'solid'
  }

  render () {
    const { appearance, buttonStyle, children } = this.props

    const buttonStyles = classnames(
      styles['button'],
      {
        [styles[appearance]]: appearance,
        [styles[buttonStyle]]: buttonStyle
      }
    )

    return (
      <button
        className={buttonStyles}
      >
        {children}
      </button>
    )
  }
}

export default Button
