import { addParameters, configure } from '@storybook/react'
import wedoTheme from './wedo-theme';

addParameters({
  options: {
    theme: wedoTheme,
  },
});

function requireAll (requireContext) {
  return requireContext.keys().map(requireContext)
}

function loadStories () {
  requireAll(require.context('..', true, /\.story.js$/))
}

configure(loadStories, module)
