import { create } from '@storybook/theming';

export default create({
  //base: 'light',

  colorPrimary: '#f83600',
  colorSecondary: '#fe8c00',

  // UI
  appBg: '#ecf0f1',
  appBorderColor: '#cccccc',
  appBorderRadius: 10,

  // Typography
  fontBase: '"Raleway", sans-serif',
  // fontCode: 'monospace',

  // Text colors
  textColor: '#7d7d7d',
  textInverseColor: '#fff',

  // Toolbar default and active colors
  // barTextColor: 'white',
  // barSelectedColor: 'white',
  barBg: '#ededed',

  // Form colors
  inputBg: 'white',
  inputBorder: 'silver',
  inputTextColor: 'black',
  inputBorderRadius: 4,

  brandTitle: 'My custom storybook',
  brandUrl: 'http://wedo.digital',
  brandImage: 'images/storybook-logo.png',
});
